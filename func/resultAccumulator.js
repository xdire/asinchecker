/**
 * Created by xdire on 8/19/16.
 */
function ResultAccumulator() {

	/**
	 * @type {{String:{status:String,code:Number}}}
	 */
	var map = {

	};

	var length = 0;
	var total = 0;

	this.length = function () {
		return length;
	};

	this.totalAccumulated = function () {
		return total;
	};

	this.append = function (key,value) {

		var status = ["ok","bad","nr"];

		if(value == 404) {
			map[key] = {status : status[1], code: value}
		} else if (value == 200) {
			map[key] = {status : status[0], code: value}
		} else {
			map[key] = {status : status[2], code: value}
		}

		length++;
		total++;

	};

	this.getResults = function () {
		return map;
	};

	this.flushResults = function () {
		if(length > 0) {
			length = 0;
			var current = clone(map);
			map = {};
			return current;
		} return {};
	};

	function clone(object) {

		var obj = {};
		for(var k in object) {
			if(typeof obj[k] === "object")
				obj[k] = clone(object[k]);
			else
				obj[k] = object[k];
		}

		return obj;

	}

}
module.exports = new ResultAccumulator();