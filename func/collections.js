function BlockingQueue() {

	var lock = false;
	var start = null;
	var end = null;
	var length = 0;

	var locks = 0;
	var lockMax = 8;

	this.concurrents = function (amount) {
		lockMax = amount;
	};

	this.isLocked = function () {
		return locks >= lockMax;
	};

	/**
	 * @param value
	 */
	this.push = function (value) {

		if(end !== null) {
			let node = new Node(value);
			end.next = node;
			end = node;
			length++;
		} else {
			let node = new Node(value);
			end = node;
			start = node;
			length++;
		}

	};

	/**
	 * Will return false on lock
	 * Will return Node on unlock
	 *
	 * @returns {* | boolean}
	 */
	this.pop = function () {

		if(!lock) {

			let value = start !== null ? start.value : null;

			if (start !== null) {
				start = start.next;
				length--;

				if(locks++ >= lockMax) {
					lock = true;
				}
			}

			return value;

		}

		return false;

	};

	this.unlock = function () {
		locks--;
	};

	/**
	 * @returns {number}
	 */
	this.size = function () {
		return length;
	};

	/**
	 * @returns {boolean}
	 */
	this.isEmpty = function () {
		return length == 0;
	};

}

function Node(value) {
	this.next = null;
	this.value = value;
}

module.exports.BlockingQueue = new BlockingQueue();