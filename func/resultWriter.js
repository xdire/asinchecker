/**
 * Created by xdire on 8/19/16.
 */
var fs = require('fs');

function ResultWriter() {

	var fileName = null;
	var stream = null;

	this.init = function () {
		var date = new Date();
		var dfmt = date.getYear()+""+date.getMonth()+""+date.getDay()+""+date.getHours()+""+date.getMinutes();
		fileName = "report"+dfmt+".txt";
		stream = fs.createWriteStream(fileName,{flags: 'a',autoClose: true});
	};

	/**
	 * @param {ResultAccumulator} resultAccumulator
	 */
	this.writeResults = function (resultAccumulator, callback) {

		//console.log(resultAccumulator.getResults());
		//console.log(resultAccumulator.length());

		var results = resultAccumulator.flushResults();

		for(var k in results) {
			//console.log("WILL WRITE: "+ k+","+results[k].status+","+results[k].code+"\n");
			stream.write(k+","+results[k].status+","+results[k].code+"\n");
		}

		if(typeof callback === "function"){
			callback();
		}

		//stream.once('open', function(fd) {

			/*for(var k in results) {
				console.log("WILL WRITE: "+ k+","+results[k].status+","+results[k].code+"\n");
				stream.write(k+","+results[k].status+","+results[k].code+"\n");
			}

			if(typeof callback === "function"){
				callback();
			}*/

		//});

	};

	this.end = function () {
		stream.end();
	};

	this.getLastFileName = function () {
		return fileName;
	};

}

module.exports = new ResultWriter();
