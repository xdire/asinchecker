var fs = require('fs');
var csv = require('csv');

function FileParse() {

	this.getFile = function (fileName, callback) {

		fs.readFile(fileName, 'utf8', function (err, contents) {

			csv.parse(contents, function (err, data) {

				if (err) {

					throw new Error("Cannot parse file");

				} else {

					callback(data);

				}

			})

		});

	}

}

module.exports = new FileParse();