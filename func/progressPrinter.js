/**
 * Created by xdire on 8/22/16.
 */
function ProgressPrinter() {

	var bar = new Uint8Array(100);

	var barPointer = 0;

	this.printAsProgress = function (elementNo, ofElements, stringPrefix = null) {

		var current = parseInt((elementNo / ofElements) * 100);

		if(current > barPointer) {
			fillBar(barPointer, current);
		}

		process.stdout.write("Progress  -> ["+current+"%] ["+elementNo+" of "+ofElements+"] : ["+getBarAsString()+"]\x1b[0G");

	};

	this.finish = function () {
		var current = 100;
		fillBar(barPointer, current);
		process.stdout.write("Progress "+current+"% : ["+getBarAsString()+"]\x1b[0G");
	};

	function fillBar(start,end) {
		var i = start;
		for(;i < bar.length; i++) {
			if(i <= end) {
				bar[i] = 1;
			} else
				break;
		}
		barPointer=i;
	}

	function getBarAsString() {

		var string = "";

		for(var i=0; i < bar.length; i++) {
			if(bar[i] == 1)
				string += "\u25A0";
			else
				string += " ";
		}

		return string;

	}

}

module.exports = new ProgressPrinter();
