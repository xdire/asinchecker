/**
 * Created by xdire on 8/18/16.
 */
function Argvs() {

	// Private member
	var actionOn = {
		file: null,
		asin: null,
		help: null,
		test: null
	};

	// Public Setters
	this.setActionOnAsin = function (action) {
		actionOn.asin = action;
	};

	this.setActionOnFile = function (action) {
		actionOn.file = action;
	};

	this.setActionOnHelp = function (action) {
		actionOn.help = action;
	};

	this.setActionOnTest = function (action) {
		actionOn.test = action;
	};

	var map = {t: [], asin: [], file: "", h: ""};

	var mode = null;

	// Public method
	this.parse = function(args) {

		// Parse identificator

		for(var i=0; i<args.length; i++) {

			if(i == 0) {

				let cur = args[i];

				if(cur.charAt(0) == '-') {
					if(cur.charAt(1) == 't')
							mode = "t";
					else if (cur.charAt(1) == 'h')
							mode = "h";
				} else {
					if(cur == "asin")
							mode = "asin";
					else if (cur == "file")
							mode = "file";
				}

			}

		// Set up mode renderer

			if(i > 0) {

				if(mode == "asin") {

					distributeAsins(map.asin, args[i]);

				} else if(mode == "file") {

					map.file = args[i];
					break;

				}

			}

		}

		if(mode == "file") {
			if(typeof actionOn.file === "function"){
				actionOn.file(map.file);
			}
		}

		if(mode == "asin") {
			if(typeof actionOn.asin === "function"){
				actionOn.asin(map.asin);
			}
		}

		if(mode == "t") {
			if(typeof actionOn.test === "function"){
				actionOn.test();
			}
		}

		if(mode == "h") {
			if(typeof actionOn.help === "function"){
				actionOn.help();
			}
		}

	};

	// Private methods
	function distributeAsins(targetArray, asinString) {
		let asins = asinString.split(',');
		for(var i=0; i < asins.length; i++){
			targetArray[i] = asins[i];
		}
	}

	function assignFile(targetString, fileString) {
		targetString = fileString;
	}

}
module.exports = new Argvs();
