let http = require('https');

/**
 * Request processor
 *
 * @constructor
 */
function Process() {

	var queue = [];
	var queueSize = 0;

	var pollTime = 50;

	this.setPollTime = function (milliseconds) {
		pollTime = milliseconds;
	};

	/**
	 *
	 * @param {ProcessOptions} options
	 * @param {Function} onConnect
	 * @param {Function} onDisconnect
	 * @param {Function} onError
	 */
	this.execute = function (options, onConnect, onDisconnect = null, onError = null) {

		if(typeof options !== "undefined") {

			var opt = options.__getOptions();
			var copt = {urlsuffix: opt.urlsuffix, urlprefix: opt.urlprefix};

			var path = (opt.urlprefix.length > 0 && opt.urlsuffix.length > 0) ? opt.urlprefix + opt.urlsuffix : opt.pathname;
			var port = opt.port > 1 ? opt.port : 80;

			var httpOptions = {
				hostname: opt.hostname,
				path: path,
				method: opt.method,
				port: port,
				headers: opt.headers
			};

			try {

				var to = setTimeout(function () {

					var req = http.request(httpOptions, function (response) {

						onConnect(response, response.headers, copt);

						var data = "";

						response.on('data', function (chunk) {
							data += chunk;
						});
						response.on('end', function () {
							if (typeof onDisconnect === "function")
								onDisconnect(response, data, copt);
						});
						response.on('error', function (err) {
							if (typeof onError === "function")
								onError(response, err, copt);
						});

					});

					req.on("error", function (err) {
						if (typeof onError === "function")
							onError(req, err, copt);
					});

					req.on("timeout", function (err) {
						if (typeof onError === "function")
							onError(req, err, copt);
					});

					req.end();

				}, pollTime);

			} catch (e) {

				console.log(e);
			}

		}

	}

}

/**
 * Options constructor
 *
 * @constructor
 */
function ProcessOptions() {

	var opt = {
		hostname: "",
		pathname: "",
		urlprefix: "",
		urlsuffix: "",
		method: "",
		port: "",
		headers: {},
	};

	this.setHostname = function (host) {
		opt.hostname = host;
		return this;
	};
	this.setUrlPrefix = function (prefix) {
		opt.urlprefix = prefix;
		return this;
	};
	this.setUrlSuffix = function (suffix) {
		opt.urlsuffix = suffix;
		return this;
	};
	this.setRequestPath = function (path) {
		opt.pathname = path;
		return this;
	};
	this.asGetRequest = function () {
		opt.method = "GET";
		return this;
	};
	this.asPostRequest = function () {
		opt.method = "POST";
		return this;
	};
	this.onPort = function (port) {
		opt.port = parseInt(port);
		return this;
	};
	this.addHeader = function (name, value) {
		opt.headers[name] = value;
		return this;
	};

	this.__getOptions = function () {
		return opt;
	};

}

module.exports.executor = new Process();
module.exports.options = new ProcessOptions();