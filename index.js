#! /usr/bin/env node
var queue = require("./func/collections.js").BlockingQueue;
var asinParse = require("./func/asinparse.js");
var progressPrinter = require("./func/progressPrinter");
var fileParse = require("./func/fileparse.js");
var requestProcessor = require("./func/process.js");
var resultAccu = require("./func/resultAccumulator.js");
var resultWriter = require("./func/resultWriter.js");
var argvs = require("./func/argvs.js");

console.log("--------------------------------------------------");
console.log("------Our beautiful small app welcomes you--------");
console.log("--------------------------------------------------");

let args = process.argv.slice(2);

argvs.setActionOnFile(function (filename) {
	console.log("Parse file mode");
	console.log(filename);
	fileParse.getFile(filename,function (data) {

		var opt = requestProcessor.options;
		var exec = requestProcessor.executor;
		resultWriter.init();

		for(var i=0; i < data.length; i++) {
			queue.push(data[i][0]);
		}

		opt.setHostname("www.amazon.com").
		setUrlPrefix("/dp").
		setUrlSuffix("").
		onPort(443).
		addHeader("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36").
		addHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8").
		addHeader("Accept-Language","en-US,en;q=0.8,ru;q=0.6").
		addHeader("Cache-Control","max-age=0").
		addHeader("Connection","keep-alive").
		addHeader("Cookie","aws_lang=en; aws-target-static-id=1452283514015-332916; aws-gz=1; pN=14; s_ppv=100; amznacsleftnav-428250662=1;  x-amz-captcha-1=1471886400350155; x-amz-captcha-2=3vsUihO61iEJIDIQO97uzw==; csm-hit=C1PQKBBMMGX9TVDPNSPM+s-GBK6YXRWF8279BX71V9F|1471879987758; ubid-main=181-4613059-5383223;").
		asGetRequest();

		var execThis = function () {

			let asin = queue.pop();

			if (asin !== false) {

				opt.setUrlSuffix("/" + asin);

				exec.setPollTime(200);

				exec.execute(opt,

					// On request connect
					function (response, responseData, options) {

						if (!queue.isLocked() && !queue.isEmpty()) {
							execThis();
						}

					},  //On request end
					function (response, responseData, options) {

						progressPrinter.printAsProgress(data.length - queue.size(), data.length);

						let asin = options.urlsuffix.slice(1);

						if (responseData.indexOf("To discuss automated access to Amazon data please contact") > 0) {
							resultAccu.append(asin, 503);
						} else {
							resultAccu.append(asin, response.statusCode);
						}

						if(resultAccu.length() % 50 == 0) {
							resultWriter.writeResults(resultAccu);
						}

						if (resultAccu.totalAccumulated() == data.length) {

							progressPrinter.finish();

							resultWriter.writeResults(resultAccu, function () {
								console.log("\nNew file created: " + resultWriter.getLastFileName());
							});

							resultWriter.end();

						}

						queue.unlock();
						response.destroy();

					}.bind(i),
					// On error
					function (response, err, options) {

						//console.log("\n\nError happened in request\n");
						//console.log(e);
						let asin = options.urlsuffix.slice(1);

						progressPrinter.printAsProgress(data.length - queue.size(), data.length);

						resultAccu.append(asin, 500);

						if(resultAccu.length() % 50 == 0) {
							resultWriter.writeResults(resultAccu);
						}

						if (resultAccu.totalAccumulated() == data.length) {

							progressPrinter.finish();

							resultWriter.writeResults(resultAccu, function () {
								console.log("\nNew file created: " + resultWriter.getLastFileName());
							});

							resultWriter.end();

						}

						queue.unlock();

						if(typeof response.destroy === "function")
							response.destroy();
						else if (typeof response.abort === "function")
							response.abort();

						if (!queue.isLocked() && !queue.isEmpty()) {
							//console.log("Exec happened on error");
							execThis();
						}

					}

				);


			}

		};

		if (!queue.isEmpty()) {
			execThis();
		}

	});

});

argvs.setActionOnAsin(function (asinList) {

	console.log("\nParse ASIN list mode\n");

	var opt = requestProcessor.options;
	var exec = requestProcessor.executor;

	opt.setHostname("www.amazon.com").
	setUrlPrefix("/dp").
	setUrlSuffix("").
	onPort(443).
	addHeader("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36").
	addHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8").
	addHeader("Accept-Language","en-US,en;q=0.8,ru;q=0.6").
	addHeader("Cache-Control","max-age=0").
	addHeader("Connection","keep-alive").
	addHeader("Cookie","aws_lang=en; aws-target-static-id=1452283514015-332916; pN=14; s_ppv=100; amznacsleftnav-428250662=1;  x-amz-captcha-1=1471886400350155; x-amz-captcha-2=3vsUihO61iEJIDIQO97uzw==; csm-hit=C1PQKBBMMGX9TVDPNSPM+s-GBK6YXRWF8279BX71V9F|1471879987758;").
	asGetRequest();

	for(var i=0; i<asinList.length; i++) {

		opt.setUrlSuffix("/"+asinList[i]);

		exec.execute(opt, function (responseCode, responseData, options) {

			var asin = options.urlsuffix.slice(1);

			},
			// On data recieve
			function (responseCode, responseData, options) {

				progressPrinter.printAsProgress(this,asinList.length);

				var asin = options.urlsuffix.slice(1);

				if(responseData.indexOf("To discuss automated access to Amazon data please contact") > 0) {
					resultAccu.append(asin,503);
				} else {
					resultAccu.append(asin,responseCode);
				}

				if(resultAccu.length() == asinList.length) {

					progressPrinter.finish();

					resultWriter.writeResults(resultAccu, function () {
						console.log("\nNew file created: "+resultWriter.getLastFileName());
					});

				}

			}.bind(i),
			// On error
			function (e, options)  {

				//console.log("\n\nError happened in request\n");
				//console.log(e);
				var asin = options.urlsuffix.slice(1);
				resultAccu.append(asin,500);

				progressPrinter.printAsProgress(this,asinList.length);

				if(resultAccu.length() == asinList.length) {
					resultWriter.writeResults(resultAccu, function () {
						console.log("\nNew file created: "+resultWriter.getLastFileName());
					});
				}

			}

		);

	}

});

argvs.parse(args);